FROM alpine:3.7

LABEL maintainer "https://github.com/blacktop"

RUN apk add --no-cache openjdk8-jre su-exec bash git libzmq

ENV VERSION 6.4.0
ENV URL "https://artifacts.elastic.co/downloads/logstash"
ENV TARBALL "$URL/logstash-${VERSION}.tar.gz"
ENV TARBALL_ASC "$URL/logstash-${VERSION}.tar.gz.asc"
ENV TARBALL_SHA "b496ed0746ee38a375a3efc5eb93677accb61c3482550a75cf9ad5e7b8e104eb8560bce79325ce85a27d22b664299edf1970452837189fcfa9310798c58825c1"
ENV GPG_KEY "46095ACC8548582C1A2699A9D27D666CD88E42B4"

RUN apk add --no-cache -t .build-deps wget ca-certificates gnupg openssl \
  && set -ex \
  && cd /tmp \
  && wget --progress=bar:force -O logstash.tar.gz "$TARBALL"; \
  if [ "$TARBALL_SHA" ]; then \
  echo "$TARBALL_SHA *logstash.tar.gz" | sha512sum -c -; \
  fi; \
  \
  if [ "$TARBALL_ASC" ]; then \
  wget --progress=bar:force -O logstash.tar.gz.asc "$TARBALL_ASC"; \
  export GNUPGHOME="$(mktemp -d)"; \
  ( gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$GPG_KEY" \
  || gpg --keyserver pgp.mit.edu --recv-keys "$GPG_KEY" \
  || gpg --keyserver keyserver.pgp.com --recv-keys "$GPG_KEY" ); \
  gpg --batch --verify logstash.tar.gz.asc logstash.tar.gz; \
  rm -rf "$GNUPGHOME" logstash.tar.gz.asc || true; \
  fi; \
  tar -xzf logstash.tar.gz \
  && mv logstash-$VERSION /usr/share/logstash \
  && adduser -DH -s /sbin/nologin logstash \
  && rm -rf /tmp/* \
  && apk del --purge .build-deps \
  && git clone --depth=1 https://github.com/dtag-dev-sec/listbot /etc/listbot \
  && wget http://geolite.maxmind.com/download/geoip/database/GeoLite2-ASN.tar.gz \
  && tar xvfz GeoLite2-ASN.tar.gz --strip-components=1 -C /usr/share/logstash/vendor/bundle/jruby/2.3.0/gems/logstash-filter-geoip-5.0.3-java/vendor/ \
  && rm GeoLite2-ASN.tar.gz

RUN apk add --no-cache libc6-compat \
       py-pip \
       py-setuptools \
       supervisor \
       rsync \
       dtach \
       curl

ENV PATH /usr/share/logstash/bin:/sbin:$PATH
ENV LS_SETTINGS_DIR /usr/share/logstash/config
ENV LANG='en_US.UTF-8' LC_ALL='en_US.UTF-8'

RUN set -ex; \
  if [ -f "$LS_SETTINGS_DIR/log4j2.properties" ]; then \
  cp "$LS_SETTINGS_DIR/log4j2.properties" "$LS_SETTINGS_DIR/log4j2.properties.dist"; \
  truncate -s 0 "$LS_SETTINGS_DIR/log4j2.properties"; \
  fi
#Alerta
RUN pip install --upgrade pip
RUN pip install alerta

# Plugins
RUN /usr/share/logstash/bin/logstash-plugin install logstash-input-http_poller
RUN /usr/share/logstash/bin/logstash-plugin install logstash-input-exec
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-json_encode
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-translate
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-cidr
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-prune
RUN /usr/share/logstash/bin/logstash-plugin install logstash-output-exec
RUN /usr/share/logstash/bin/logstash-plugin install logstash-output-syslog
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-geoip

VOLUME ["/usr/share/logstash/config", "/usr/share/logstash/pipeline"]

RUN  mkdir -p /.backup/logstash/config/
RUN mkdir /.backup/logstash/pipeline/

COPY config/logstash /.backup/logstash/config
COPY config/pipeline /.backup/logstash/pipeline
RUN chown -R logstash /.backup/logstash \
    /etc/listbot
COPY restore-config.sh /run/
COPY logstash-entrypoint.sh /
COPY ./supervisord.conf /supervisord.conf /
COPY ./minemeld_cron /etc/crontabs/root
COPY ./get_oa_devices.py /

ENV LOGSTASH_PWD="changeme" \
    ELASTICSEARCH_HOST="es-client-1" \
    ELASTICSEARCH_PORT="9200" \
    HEAP_SIZE="2g" \
    TS_PWD="changeme" \
    RABBITMQ_USER="admin" \
    RABBITMQ_PASSWORD="changeme" \
    ALERTA_HOST="alerta"

ENTRYPOINT ["/logstash-entrypoint.sh"]
CMD ["/usr/bin/supervisord", "-n", "-c", "/supervisord.conf"]
