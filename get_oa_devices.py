#!/usr/bin/python
from urlparse import urlunparse
import json
import requests
import pickle
import os.path
import os

OA_LOGIN = os.environ.get('OPEN_AUDIT_LOGIN', 'admin')
OA_PASSWORD = os.environ.get('OPEN_AUDIT_PASSWORD', 'password')
OA_HOST = os.environ.get('OPEN_AUDIT_HOST', 'localhost')
COOKIES_FILE = '/tmp/cookies.pkl'

session = requests.Session()


def make_url(path, query=None):
    return urlunparse(('http', OA_HOST, path, None, query, None))


def load_session():
    if os.path.isfile(COOKIES_FILE):
        with open(COOKIES_FILE) as f:
            cookies = requests.utils.cookiejar_from_dict(pickle.load(f))
            session.cookies=cookies


def save_session():
    with open(COOKIES_FILE, 'w') as f:
        pickle.dump(requests.utils.dict_from_cookiejar(session.cookies), f)


def oa_auth():
    r = session.post(
        make_url('en/omk/open-audit/login'),
        data={
            'username': OA_LOGIN,
            'password': OA_PASSWORD,
        }
    )
    if r.status_code != 200:
        raise RuntimeError('Auth failed: %s' % r.status_code)


def get_devices(try_count=1):
    r = session.get(make_url('omk/open-audit/devices', query='format=json'))
    while try_count:
        try_count -= 1
        if r.status_code == 401:
            oa_auth()
            r = session.get(make_url('omk/open-audit/devices', query='format=json'))
    if r.status_code != 200:
        raise RuntimeError('An error occurred while running the query: %s' % r.status_code)
    return r.json()


load_session()
print(json.dumps(get_devices()))
save_session()
